require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  'extends': [
    'plugin:vue/vue3-essential',
    '@vue/eslint-config-standard',
    '@vue/eslint-config-prettier/skip-formatting',
    './.eslintrc-auto-import.json'
  ],
  'rules':{
    'vue/multi-word-component-names': "off",
    'camelcase': 'off'
  },
  globals: {
    uni: true,
    wx: true
  }
}
