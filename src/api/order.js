import { request } from '@/utils';
export function orderCreateApi(data) {
  return request({
    url: '/api/order/goods',
    method: 'POST',
    data,
  });
}
export function orderPayApi(data) {
  return request({
    url: '/api/order/pay',
    method: 'POST',
    data,
  });
}
export function orderListApi(data) {
  return request({
    url: '/api/order/index',
    data,
  });
}
export function orderInfoApi(data) {
  return request({
    url: '/api/order/info',
    data,
  });
}
export function orderCancelApi(data) {
  return request({
    url: '/api/order/cancel',
    method: 'POST',
    data,
  });
}
export function orderConfirmApi(data) {
  return request({
    url: '/api/order/confirm',
    method: 'POST',
    data,
  });
}

export function orderExpressApi(data) {
  return request({
    url: '/api/order/queryExpress',
    data,
  });
}
