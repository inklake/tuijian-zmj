import { request } from '@/utils';

export function bannerList() {
  return request({
    url: '/api/banner/index',
  });
}
export function blockList() {
  return request({
    url: '/api/block/index',
  });
}
export function noticeList() {
  return request({
    url: '/api/notice/index',
  });
}
export function noticeInfo(data) {
  return request({
    url: '/api/notice/info',
    data,
  });
}
