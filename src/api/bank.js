import { request } from '@/utils';

export function bankListApi() {
  return request({
    url: '/api/bankcard/index',
  });
}
export function bankDelApi(data) {
  return request({
    url: '/api/bankcard/del',
    method: 'POST',
    data,
  });
}
export function bankEditApi(data) {
  return request({
    url: '/api/bankcard/edit',
    method: 'POST',
    data,
  });
}
export function bankDefaultApi(data) {
  return request({
    url: '/api/bankcard/setDefault',
    method: 'POST',
    data,
  });
}
export function bankInfoApi(data) {
  return request({
    url: '/api/bankcard/info',
    data,
  });
}
