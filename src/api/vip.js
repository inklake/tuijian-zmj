import { request } from '@/utils';

/**
 * 获取会员权益列表
 * @returns {Promise<unknown>}
 */
export function vipEquityList() {
  return request({
    url: '/api/vip/index',
  });
}

/**
 * 获取会员权益配置
 * @returns {Promise<unknown>}
 */
export function vipEquityConfigApi() {
  return request({
    url: '/api/vip/chargeConfig',
  });
}

export function vipChargeApi(data) {
  return request({
    url: '/api/order/charge',
    method: 'POST',
    data,
  });
}

export function vipWageLogApi(data) {
  return request({
    url: '/api/user/wageLog',
    data,
  });
}
export function vipPrizeLogApi() {
  return request({
    url: '/api/user/prizeLog',
  });
}
