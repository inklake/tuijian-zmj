import { request } from '@/utils';

export function goodsListApi(data) {
  return request({
    url: '/api/goods/index',
    method: 'GET',
    data,
  });
}
export function goodsInfoApi(data) {
  return request({
    url: '/api/goods/info',
    data,
  });
}
