import { request } from '@/utils';
export function loginByCode(data) {
  return request({
    url: '/api/user/loginByCode',
    method: 'POST',
    data,
  });
}
export function userInfo() {
  return request({
    url: '/api/user/info',
  });
}
export function getPhoneNumber(data) {
  return request({
    url: '/api/user/getPhoneNumber',
    method: 'POST',
    data,
  });
}
export function userRegister(data) {
  return request({
    url: '/api/user/register',
    method: 'POST',
    data,
  });
}
export function userInfoEdit(data) {
  return request({
    url: '/api/user/edit',
    method: 'POST',
    data,
  });
}

export function userList(params) {
  return request({
    url: '/user',
    data: params,
  });
}

export function userQrcodeApi(data) {
  return request({
    url: '/api/user/getMpCode',
    data,
  });
}

export function userTransferApi(data) {
  return request({
    url: '/api/user/transfer',
    method: 'POST',
    data,
  });
}

export function userTransferLogApi(data) {
  return request({
    url: '/api/user/transferLog',
    data,
  });
}

export function userWithdrawApi(data) {
  return request({
    url: '/api/user/withdraw',
    method: 'POST',
    data,
  });
}
export function userWithdrawLogApi(data) {
  return request({
    url: '/api/user/withdrawLog',
    data,
  });
}

export function userAgreementApi() {
  return request({
    url: '/api/vip/agreement',
  });
}
export function questionListApi() {
  return request({
    url: '/api/question/index',
  });
}

export function userSuggestApi(data) {
  return request({
    url: '/api/suggestion/submit',
    method: 'POST',
    data,
  });
}
