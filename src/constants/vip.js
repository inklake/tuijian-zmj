export const VIP_LIST = {
  1: {
    level: 1,
    icon: '/static/images/vip/v1-icon.png',
    logo: '/static/images/vip/v1.png',
    subtitleColor: '#A6997D',
    rightBottomColor: '#F2ECCA',
    defaultProgressBackground: 'linear-gradient(180deg, #646464 0%, #424242 100%)',
    activeProgressBackground:
      'linear-gradient(270deg, #FFECC5 0%, #FFC384 17%, #FFC07F 86%, #F8EDD1 100%)',
  },
  2: {
    level: 2,
    icon: '/static/images/vip/v2-icon.png',
    logo: '/static/images/vip/v2.png',
    subtitleColor: '#AF7547',
    rightBottomColor: '#887040',
    defaultProgressBackground: 'linear-gradient(180deg, #DDA879 0%, #DBB38C 100%)',
    activeProgressBackground: 'linear-gradient(270deg, #FF7347 0%, #E56642 100%)',
  },
  3: {
    level: 3,
    icon: '/static/images/vip/v3-icon.png',
    logo: '/static/images/vip/v3.png',
    subtitleColor: '#A79CE7',
    rightBottomColor: '#8E7FE2',
    defaultProgressBackground: 'linear-gradient(180deg, #A7ABDE 0%, #A69DEC 100%)',
    activeProgressBackground: 'linear-gradient(180deg, #8857F8 0%, #6400FF 100%)',
  },
};

// 会员最大等级
export const VIP_MAX_LEVEL = 3;

export const MARKET_ENUM = {
  1: '市场一',
  2: '市场二',
};
