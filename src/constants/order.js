export const ORDER_STATE = {
  0: {
    order_status: 0,
    name: '全部',
  },
  1: { order_status: 1, name: '待付款', color: '#FF7855' },
  2: { order_status: 2, name: '侍收货', color: '#011FFE' },
  3: { order_status: 3, name: '已完成', color: '#28C100' },
  9: { order_status: 9, name: '已取消' },
};

export const ORDER_COUNT_TIME = 24 * 3600; // 未支付订单倒计时，默认24h
