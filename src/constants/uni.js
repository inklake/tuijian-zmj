export const UNI_COLOR = {
  primary: '#E6212A',
  success: '#1dbb67',
  warning: '#FF8818',
  error: '#e32e2e',
  info: '#8f939c',
  blue: '#1679FF',
};

export const UNI_ATTR_KEY = ['text', 'bg', 'border'];

export const UNI_COLORS = () => {
  const result = {};
  for (const [key, value] of Object.entries(UNI_COLOR)) {
    for (const item of UNI_ATTR_KEY) {
      result[`${item}-${key}`] = `${item}-${value}`;
    }
  }
  return result;
};
