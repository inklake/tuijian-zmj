import { BASE_API_PREFIX } from '@/config';
import { orderPayApi } from '@/api';
import { getToken } from '@/utils/auth';

export function getFullPath(url) {
  return BASE_API_PREFIX + url;
}

export function upload(filePath) {
  return new Promise((resolve) => {
    uni.uploadFile({
      url: BASE_API_PREFIX + '/api/common/upload', // config.appUpload
      filePath,
      name: 'file',
      success: (res) => {
        const { data } = JSON.parse(res.data);
        resolve(data?.url || '');
      },
      fail: (err) => {
        console.log(err);
      },
      complete: () => {},
    });
  });
}

export function sortBy(arr, rules) {
  return arr.sort((a, b) => {
    for (let i = 0; i < rules.length; i++) {
      const [key, direction] = rules[i];
      const order = direction === 'desc' ? -1 : 1;
      if (a[key] < b[key]) {
        return -1 * order;
      }
      if (a[key] > b[key]) {
        return order;
      }
    }
    return 0;
  });
}

export async function createOrderPay(order_id, pay_method, needJump = true, good_type = '1') {
  pay_method = +pay_method;
  const res = await orderPayApi({
    order_id,
    pay_method,
  });
  if (res === undefined) return;

  if (pay_method === 1) {
    wx.requestPayment({
      ...res.payinfo,
      success(res) {
        if (!needJump) {
          uni.$emit('paySuccess', good_type);
          return;
        }
        needJump &&
          uni.navigateTo({
            url: '/pages/order/index',
            success: function (res) {
              // 通过eventChannel向被打开页面传送数据
              res.eventChannel.emit('orderState', { data: good_type === '2' ? 3 : 2 });
            },
          });
      },
      fail(e) {
        uni.$u.toast('用户取消支付');
        needJump &&
          uni.navigateTo({
            url: '/pages/order/index',
            success: function (res) {
              // 通过eventChannel向被打开页面传送数据
              res.eventChannel.emit('orderState', { data: 1 });
            },
          });
      },
    });
  } else {
    needJump &&
      uni.navigateTo({
        url: '/pages/order/index',
        success: function (res) {
          // 通过eventChannel向被打开页面传送数据
          res.eventChannel.emit('orderState', { data: good_type === '2' ? 3 : 2 });
        },
      });
  }
}

export async function download(filePath) {
  uni.saveImageToPhotosAlbum({
    filePath,
    success() {
      uni.showToast({
        icon: 'success',
        title: '保存成功！',
      });
    },
    fail() {
      uni.showToast({
        icon: 'none',
        title: '保存失败，请稍后再试~',
      });
    },
  });
}

export function queryURLParams(url) {
  const askIn = url.indexOf('?');
  let wellIn = url.indexOf('#');
  let askText = '';
  let wellText = '';
  // #不存在
  // eslint-disable-next-line no-unused-expressions
  wellIn === -1 ? (wellIn = url.length) : null;
  // ?存在
  // eslint-disable-next-line no-unused-expressions
  askIn >= 0 ? (askText = url.substring(askIn + 1, wellIn)) : null;
  wellText = url.substring(wellIn + 1);
  const result = {};
  // eslint-disable-next-line no-unused-expressions
  wellText !== '' ? (result.HASH = wellText) : null;
  if (askText !== '') {
    const ary = askText.split('&');
    ary.forEach((item) => {
      const aryText = item.split('=');
      result[aryText[0]] = aryText[1];
    });
  }
  return result;
}

export function checkLogin() {
  if (getToken()) return true;
  uni.showModal({
    title: '提示',
    content: '请先登录并完善个人信息',
    cancelText: '暂不登录',
    confirmText: '立即完善',
    success: function (res) {
      if (res.confirm) {
        uni.$u.route('/pages/user/edit');
      }
    },
  });
}
