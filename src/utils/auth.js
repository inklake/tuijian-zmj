import { STORAGE_KEY_PREFIX } from '@/config';

const TokenKey = STORAGE_KEY_PREFIX + 'token';

// 认证令牌
export function getToken() {
  return uni.getStorageSync(TokenKey);
}

export function setToken(token) {
  return uni.setStorageSync(TokenKey, token);
}

export function removeToken() {
  return uni.removeStorageSync(TokenKey);
}
