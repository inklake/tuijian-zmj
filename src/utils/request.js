// 全局请求封装
import { getFullPath, getToken, removeToken } from '@/utils';

export function request(params) {
  const { url, method = 'get', data = {} } = params;

  let header = {};
  if (method.toLocaleLowerCase() === 'post') {
    header = {
      'Content-Type': 'application/json',
    };
  }
  // 获取本地token
  if (getToken()) {
    header.token = getToken();
  }
  return new Promise((resolve, reject) => {
    uni.showLoading({ mask: true });
    uni.request({
      url: getFullPath(url),
      method,
      header,
      data,
      success(response) {
        const res = response;
        // 根据返回的状态码做出对应的操作
        // 获取成功
        if (res.statusCode === 200) {
          const { code, msg } = res.data;
          if (code === 0) {
            uni.showToast({
              title: msg || '网络异常',
              icon: 'error',
              duration: 3000,
            });
            reject(msg);
          } else {
            resolve(res.data.data);
          }
        } else {
          // uni.clearStorageSync();
          switch (res.statusCode) {
            case 401:
              // uni.showModal({
              //   title: '提示',
              //   content: '请登录',
              //   showCancel: false,
              //   success(res) {
              //     uni.switchTab({
              //       url: '/pages/user/index',
              //     });
              //   },
              // });

              removeToken();
              break;
            case 404:
              uni.showToast({
                title: '请求地址不存在...',
                duration: 2000,
              });
              break;
            default:
              uni.showToast({
                title: '网络异常',
                icon: 'error',
                duration: 3000,
              });
              break;
          }
        }
      },
      fail(err) {
        if (err.errMsg.indexOf('request:fail') !== -1) {
          uni.showToast({
            title: '网络异常',
            icon: 'error',
            duration: 2000,
          });
        } else {
          uni.showToast({
            title: '未知异常',
            duration: 2000,
          });
        }
        reject(err);
      },
      complete() {
        // 不管成功还是失败都会执行
        uni.hideLoading();
        // uni.hideToast();
      },
    });
  }).catch((e) => {});
}
