/**
 * 小程序的授权情况判断
 */

/**
 * 小程序的权限scope及对应的名称
 * @type {Map<string, string>}
 */
export const MP_SETTING_AUTH = new Map([
  ['scope.userInfo', '用户信息'],
  ['scope.getPhoneNumber', '手机号码'],
  ['scope.userLocation', '地理位置'],
  ['scope.album', '相册权限'],
  ['scope.camera', '相机权限'],
  ['scope.writePhotosAlbum', '添加到相册'],
]);

export const checkAuthSetting = (authType) => {
  return new Promise((resolve) => {
    uni.getSetting({
      success(res) {
        if (!authType) resolve(res.authSetting);
        else resolve(res.authSetting[authType]);
      },
    });
  });
};

/**
 * 判断小程序的某个权限，如果未开启提示用户开启并跳转到设置页面
 * @param authType
 */
export const openSetting = (authType) => {
  const name = MP_SETTING_AUTH.get(authType);
  uni.showModal({
    title: '提示',
    content: '需要开启 ' + name + ' 权限',
    confirmText: '去开启',
    success: async function (res) {
      if (res.confirm) {
        console.log('用户点击确定');
        //
        const authSetting = await checkAuthSetting(authType);
        if (authSetting === undefined) {
          uni.authorize({
            scope: authType,
            success() {},
          });
        } else if (!authSetting) {
          uni.openSetting();
        }
        console.log('authSetting>>>>>>>>>>', authSetting);
      } else if (res.cancel) {
        console.log('用户点击取消');
      }
    },
  });
};
/**
 * 推荐使用，使用前的前置判断
 * @param type
 * @returns {Promise<boolean>}
 */
export const checkAuth = async (type) => {
  const setting = await checkAuthSetting(type);
  console.log('setting', setting);
  if (setting || setting === undefined) {
    return true;
  } else {
    openSetting(type);
    return false;
  }
};
