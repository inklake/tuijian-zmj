import { vipEquityList } from '@/api';
import { VIP_LIST } from '@/constants/vip';

const storeName = 'vip';
export const useVipStore = defineStore(storeName, () => {
  const vipList = ref([]);

  const vipDataList = computed(() =>
    vipList.value?.map((item) => ({ ...item, ...VIP_LIST[item.level] })),
  );

  async function getVipList() {
    const { list } = await vipEquityList();
    if (!(Array.isArray(list) && list.length)) return;
    vipList.value = list.map((item) => ({ ...item, ...VIP_LIST[item.level] }));
  }

  return {
    vipList,
    getVipList,
    vipDataList,
  };
});
