import { removeToken, setToken } from '@/utils';
import { loginByCode, userInfo as userInfoApi } from '@/api';

const storeName = 'user';

export const useUserStore = defineStore(storeName, () => {
  const userInfo = ref({});
  const token = computed(() => userInfo.value.token);
  function setUserInfo(data) {
    userInfo.value = data;
    data.token ? setToken(data.token) : removeToken();
  }

  async function getUserInfo() {
    const { userinfo, ...other } = await userInfoApi();
    const data = { ...userinfo, ...other };

    setUserInfo(data);
  }
  async function wxLogin() {
    const { errMsg, code } = await uni.login({
      provider: 'weixin', // 使用微信登录
    });
    if (errMsg !== 'login:ok') return;

    const { userinfo = {} } = (await loginByCode({ code })) ?? {};
    if (userinfo.token) {
      setToken(userinfo.token);
      await getUserInfo(userinfo);
    }
  }

  return {
    userInfo,
    token,
    setUserInfo,
    wxLogin,
    getUserInfo,
  };
});
