import { STORAGE_KEY_PREFIX } from '@/config';

const storeName = 'search';
export const useSearchStore = defineStore(
  storeName,
  () => {
    const searchList = ref([]);

    function getSearchIndex(data) {
      return searchList.value.indexOf(data);
    }
    function setSearch(data) {
      getSearchIndex(data) < 0 && searchList.value.push(data);
    }
    function deleteSearch(data) {
      searchList.value.splice(getSearchIndex(data), 1);
    }
    function clearSearch(data) {
      searchList.value.splice(0);
    }

    return {
      searchList,
      setSearch,
      deleteSearch,
      clearSearch,
    };
  },
  {
    unistorage: {
      key: STORAGE_KEY_PREFIX + storeName, // 缓存的键，默认为该 store 的 id，这里是 main,
      paths: ['searchList'], // 需要缓存的路径，这里设置 foo 和 nested 下的 data 会被缓存
    },
  },
);
