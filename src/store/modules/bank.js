import { bankDefaultApi, bankDelApi, bankInfoApi, bankListApi } from '@/api';

const storeName = 'bank';
export const useBankStore = defineStore(storeName, () => {
  const bankList = ref([]);
  const bankInfo = ref({});

  async function getBankList() {
    const { list } = await bankListApi();
    bankList.value = list;
  }
  async function delBank({ id }) {
    const { confirm } = await uni.showModal({
      content: '确定要删除吗？',
    });

    if (!confirm) return;
    await bankDelApi({ id });
    await getBankList();
  }
  async function setDefaultBank({ id }) {
    const { confirm } = await uni.showModal({
      content: '确定要设置为默认银行卡吗？',
    });

    if (!confirm) return;
    await bankDefaultApi({ id });
    await getBankList();
  }
  async function getBankInfo(id) {
    if (!id) {
      bankInfo.value = {};
      return;
    }
    const { info } = await bankInfoApi({ id });
    bankInfo.value = info;
  }

  return {
    bankInfo,
    bankList,
    getBankList,
    delBank,
    setDefaultBank,
    getBankInfo,
  };
});
