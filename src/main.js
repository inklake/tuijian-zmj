import { createSSRApp } from 'vue';
import * as Pinia from 'pinia';
import App from './App.vue';
import 'uno.css';
import uviewPlus from 'uview-plus';
import store from '@/store';
import { UNI_COLOR as color } from '@/constants/uni';

export function createApp() {
  const app = createSSRApp(App);
  app.use(Pinia.createPinia());
  app.use(store);
  app.use(uviewPlus);

  uni.$u.setConfig({
    color,
  });

  return {
    app,
    Pinia,
  };
}
