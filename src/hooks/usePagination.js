import { onPullDownRefresh, onReachBottom } from '@dcloudio/uni-app';

const usePagination = (apiUrl) => {
  const listData = ref([]);
  const totalData = ref(0);
  // const swiperHeight = ref(0);
  const otherData = ref({});

  // 新增pageInfo对象保存分页数据
  const pageParams = ref({
    page: 1,
    pageSize: 10,
  });
  const queryParams = ref({});

  onPullDownRefresh(initPage);
  onReachBottom(loadMore);

  function initPage(params = {}) {
    Object.assign(queryParams.value, params);
    pageParams.value.page = 1;
    listData.value.splice(0);
    getList();
  }
  function loadMore() {
    if (listData.value.length >= totalData.value) return;
    pageParams.value.page++;
    getList();
  }

  // 动态设置swiper高度
  /*  async function setSwiperHeight() {
    const query = wx.createSelectorQuery();
    query.select('.swiper-wrapper').boundingClientRect();
    query.selectViewport().scrollOffset();
    query.exec(function (res) {
      swiperHeight.value = res[1].scrollHeight;
      console.log('swiperHeight.value..>>>>', swiperHeight.value);
    });
  } */

  async function getList() {
    console.log('分页请求接口');
    const { list, total, ...other } = await apiUrl?.({ ...pageParams.value, ...queryParams.value });
    listData.value.push(...list);
    totalData.value = total;
    otherData.value = other;
    // setTimeout(() => {
    //   setSwiperHeight();
    // }, 500);
    uni.stopPullDownRefresh();
  }
  return {
    initPage,
    listData,
    pageParams,
    otherData,
  };
};
export default usePagination;
